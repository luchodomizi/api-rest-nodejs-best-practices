import express from 'express'

// Routers
import v1UsersRouter from '@/routes/v1/userRoutes'

import { PORT } from '@/config/environment'

const app = express()

app.use(express.json())
app.use('/api/v1/users', v1UsersRouter)

app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`)
})
