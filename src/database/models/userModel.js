import DB from '@/database/db/db.json'

const getAllUsers = () => {
  return DB.users
}

const userModel = {
  getAllUsers
}

export default userModel
