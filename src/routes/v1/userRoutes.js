import express from 'express'
import userController from '@/controllers/userController'

const router = express.Router()
const {
  getAllUsers,
  getOneUser,
  createNewUser,
  updateOneUser,
  deleteOneUser
} = userController

router
  .get('/', getAllUsers)
  .get('/:userId', getOneUser)
  .post('/', createNewUser)
  .put('/:userId', updateOneUser)
  .delete('/:userId', deleteOneUser)

export default router
