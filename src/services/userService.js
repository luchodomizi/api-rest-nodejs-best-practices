import userModel from '@/database/models/userModel'

const getAllUsers = () => {
  const users = userModel.getAllUsers()
  return users
}

const getOneUser = (userId) => {
  return { message: `GET user with id ${userId}` }
}

const createNewUser = (body) => {
  return { message: 'POST new user', user: body }
}

const updateOneUser = (userId) => {
  return { message: `User with id ${userId} updated` }
}

const deleteOneUser = (userId) => {
  return { message: `User with id ${userId} deleted` }
}

const userService = {
  getAllUsers,
  getOneUser,
  createNewUser,
  updateOneUser,
  deleteOneUser
}

export default userService
