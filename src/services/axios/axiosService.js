import axios from 'axios'
import { API_URL } from '@/config/environment'

const axiosService = axios.create({
  baseURL: API_URL, // Add your API URL in .env file and import from @/config/envirnment.js
  timeout: 2000,
  headers: {
    'Content-Type': 'application/json'
  }
})

export default axiosService
