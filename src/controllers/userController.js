import userService from '@/services/userService'

const getAllUsers = (req, res) => {
  const allUsers = userService.getAllUsers()
  res.send(allUsers)
}

const getOneUser = (req, res) => {
  const user = userService.getOneUser(req.params.userId)
  res.send(user)
}

const createNewUser = (req, res) => {
  // Use validations here

  const newUser = req.body

  const createdUser = userService.createNewUser(newUser)
  res.send(createdUser)
}

const updateOneUser = (req, res) => {
  const updatedUser = userService.updateOneUser(req.params.userId)
  res.send(updatedUser)
}

const deleteOneUser = (req, res) => {
  userService.deleteOneUser(req.params.userId)
  res.send('User deleted')
}

const userController = {
  getAllUsers,
  getOneUser,
  createNewUser,
  updateOneUser,
  deleteOneUser
}

export default userController
