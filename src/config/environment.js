import 'dotenv/config'

export const { PORT = 3000, API_URL } = process.env
