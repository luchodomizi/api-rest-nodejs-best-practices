module.exports = {
  presets: [
    ['@babel/preset-env', {
      targets: {
        node: 'current'
      }
    }]
  ],
  plugins: [
    ['module-resolver', {
      root: ['./'],
      alias: {
        '@': './src' // It defines the alias @ to the src folder
      }
    }]
  ]
}
